# Whitebox

![Whitebox logo](branding/whitebox_logo_600x450.png)

## Description

Whitebox is a flight recording and sharing system for small airplanes, enhancing the flight experience by allowing pilots and passengers to record, review, and share flight data, communications, and media. It can also be used to review training flights during debrief, taking care of synchronizing and uploading the flight data to the cloud.

It's an open source counterpart to the black boxes airplanes have to record all information about a flight.

## Certification

It is an uncertified system which is not directly connected to the airplane systems, much like a tablet running Foreflight or SkyDemon. It can thus be brought in with you in any general aviation airplane. Being also open source, it can be freely accessed and modified.

## Technical description

Whitebox is a small computer which you bring with you in the cockpit during flight, and which connects to various additional recording devices, such as a 360 camera or the passengers phones.

It can be accessed by the pilot and the passengers, from tablets or phones over wifi (or your desktop computer after the flight), using the devices' browser.

## Roadmap

Features being considered (TBD):

- Video recording and playback:
    - 360 camera external view
    - Cockpit interior cameras views
    - Control the 360 camera orientation during the flight
    - Passenger phone recordings synchronization (photos/videos), tagged to specific flight times/locations
    - Automatically generate a video to share, showing the highlights of the flight (tagged during the flight)
- Automatic recording and transcription of radio and in-cockpit communications
- GPS position tracking and 3D route visualization (like/via FlySto)
- Note-taking for in-flight observations (instructions, comments, remarks by passengers)
- Automated synchronization of data to the cloud - bring the Whitebox within reach of a wifi connection and it will happen automatically
- Community contribution platform (public/forum/wiki) for sharing flight experiences:
    - A page dedicated to each flight, with access permission management
    - Link and embed flight recordings in external platforms (e.g., YouTube), including deep-linking to a specific section of the flight
- Pre-flight briefing and post-flight debriefing
    - Review critical phases of flights
    - Integration with external data sources (OpenFlightMaps, Flypedia) for airport and flight procedure information sharing: use data to share and discover information about airports, flight procedures and practices, radio recordings...

## Installation & Usage

Whitebox is currently in development. Being an open source project, all elements are available here, but it is not ready for installation and usage just yet. If you would like to participate as one of the early testers though, write to contact@whitebox.aero.

## Support

If you find an issue with the software, please [open a bug report ticket](https://gitlab.com/whitebox-aero/whitebox/-/issues/new), or write to support@whitebox.aero. 

You can also come [chat with us on Matrix (#whitebox-aero:matrix.org)](https://matrix.to/#/#whitebox-aero:matrix.org)

## Contributing

To contribute, feel free to open a merge request, or reach out to contact@whitebox.aero.

## License

The project is released under the AGPLv3.
